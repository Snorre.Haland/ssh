#oppgave 1
"""
"""
def sjekkPoly(x,i):
	xs = ["x^2","x",""]
	if x == 1 and i != 2:
		return f"{xs[i]} + "
	return f"{x}{xs[i]} + "

def poly_string(a,b,c):
	returnString = ""
	for i, x in enumerate([a,b,c]):
		if x == 0:
			continue
		returnString += sjekkPoly(x,i)
	returnString = returnString[0:-3]
	if returnString == "":
		returnString="0"
	return returnString

print(poly_string(2,1,1))
#oppgave 2

def sjekkPoly(x,i):
	xs = ["x^2","x",""]
	returnString = ""
	if x > 0:
		returnString = " + "
	else:
		returnString = " - "
	if x == 1:
		returnString += f"{xs[i]}"
	elif x == -1 and i != 2:
		returnString += f"-{xs[i]}"
	elif i == 0:
		returnString += f"{x}{xs[i]}"
	else:
		returnString += f"{abs(x)}{xs[i]}"
	return returnString


def poly_string(a,b,c):
	returnString = ""
	for i, x in enumerate([a,b,c]):
		if x == 0:
			continue
		returnString += sjekkPoly(x,i)
	returnString = returnString[3:]
	if returnString == "":
		returnString="0"
	return returnString

print(poly_string(-2,-3,-1))

#oppgave 3
enhet = ["nm","THz"]
verdier = [[380,740],[405,790]]
while True:
	valg = int(input(f"Velg en enhet {enhet}: "))
	if valg in enhet:
		break
farge = {"rød": [[625],[]]}

#oppgave 4
år = int(input("velg et år: "))
print((lambda z: True if (z % 400 == 0) else (False if (z % 100 == 0) else(True if (z % 4 == 0) else False)))(år))